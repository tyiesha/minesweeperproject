
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Random;


/**
 * This class represents a Minesweeper game.
 *
 * @author TYIESHA HOLMES <tmh37108@uga.edu>
 */
public class Minesweeper {
	
	//variables
	public static Scanner seed = null;
	public static int roundsCompleted = 0;
	public static int rows;
	public static  int cols;
	public int score;
	public Boolean isRunning;
	public static Boolean nofog = false; //for nofog command
	public static Boolean seedFileGame = false;
	public static Boolean begin;
	public static Boolean initializeArray = true;
	public static int reveal = -4;
	public static int numOfMines;
	public static int mineMarker = -1;
	public static int markFlag = -2;
	public static int guessFlag = -3;
	public static int mineCounter; //used to count adjacent mines
	public static int[][] gameboard;
	public static int[][] gameboardMines;

    /**
     * Constructs an object instance of the {@link Minesweeper} class using the
     * information provided in <code>seedFile</code>. Documentation about the 
     * format of seed files can be found in the project's <code>README.md</code>
     * file.
     *
     * @param seedFile the seed file used to construct the game
     * @see  <a href="https://github.com/mepcotterell-cs1302/cs1302-minesweeper-alpha/blob/master/README.md#seed-files">README.md#seed-files</a>
     */
    public Minesweeper(File seedFile) 
    {
    	seedFileGame = true;
    	begin = true;
    	try
		{
			seed = new Scanner(seedFile);
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
    	
    	while(seed.hasNextLine() == true)
    	{
    		Minesweeper.rows = seed.nextInt();
        	Minesweeper.cols = seed.nextInt();
        	Minesweeper.numOfMines = seed.nextInt();
        	
        	//checks if rows + cols are in valid range
        	if((rows > 10) || (cols > 10) || (rows <= 0) || (cols <= 0))
        	{
        		seedFileError(seedFile);
        	}
        	
        	//check if numOfMines exceeds # of squares in grid
        	if(numOfMines > (rows * cols))
        	{
        		seedFileError(seedFile);
        	}
        	
        	break;
    	}

    } // Minesweeper

    /**
     * Constructs an object instance of the {@link Minesweeper} class using the
     * <code>rows</code> and <code>cols</code> values as the game grid's number
     * of rows and columns respectively. Additionally, 10% (rounded up) 
     * of the squares in the grid will will be assigned mines, randomly.
     *
     * @param rows the number of rows in the game grid
     * @param cols the number of cols in the game grid
     */
    public Minesweeper(int rows, int cols) 
    {
    	//check if rows and cols are in valid range; if not, display errors messages  boundsError()
    	//if everything is fine, everything that uses it should be instantiated
    	if((rows > 10) || (cols > 10) || (rows <= 0) || (cols <= 0))
    	{
    		boundsError();
    	}
    	else
    	{
    		Minesweeper.rows = rows;
    		Minesweeper.cols = cols;
    		begin = true;
    	}
    	
    } // Minesweeper
    
    /**
     * Displays the starting logo, Starts the game and execute the game loop.
     * Game loop calls the gameboard, prompt, and checkWin methods.
     */
    public void run() 
    {
    	//Minesweeper logo; display only onces
    	System.out.println(startLogo());
    	
    	isRunning = true;	
    	
    	while(isRunning == true)
    	{
    		//display()
    		gameboard();
    		//prompt()
    		prompt();
    		//checkWin()
    		checkWin();
    	}
 
    } // run
    
    /**
     * checks if the player has won the game. To win the game, all locations
     * with a mine must be marked AND all other locations must be revealed (no guesses)
     * @return win true if the player wins the game; false if not and game continues
     */
    public Boolean checkWin()
    {
    	Boolean win = false;
    	Boolean noWin = false;
    
    	//row
    	for(int x = 0; x < gameboard.length; x++)
    	{
    		//col
    		for(int y = 0; y < gameboard[x].length; y++)
    		{
    			if((((gameboard[x][y] == 0) || (gameboard[x][y] == mineMarker)) || (gameboard[x][y] == guessFlag)) || (gameboardMines[x][y] == 0 && gameboard[x][y] == markFlag))   
    			{
    				win = false;
    				noWin = true;
    			}
    			else
    			{
    				win = true;
    			}
    		}
    	}
    	
    	if((win == true) && (noWin == false))
    	{
    		roundsCompleted++;
    		playerWon();
    		isRunning = false;
    	}
    	
    	roundsCompleted++;
    	return win;
    }
    
    /**
     * Get the user's input, parses the word and integers into separate variables.
     * Then takes the first letter of each word and selects the command option the user typed.
     * "mark/m" - marks the square as definitely containing a mine
     * "quit/q" - displays goodbyeMessage()
     * "help/h" - displays help menu
     * "reveal/r" - reveals the square; if there are adjacent mines, it displays how many
     * "guess/g" - marks the square as possibly containing a mine
     * "nofog" - shows location of all mines for one round only
     */
    public static void userInput()
    {
    	String word = "";
    	int numRow = 0;
    	int numCol = 0;
    	String command = "";
	    //reads user input after minesweeper-alpha$ prompt
    	//parse "mark 10 10" into variables
    	Scanner scanner = new Scanner(System.in);
    	
    	//to parse command and store into variables [part 1]
    	if(scanner.hasNext() == true)
    	{
    		word = scanner.next();
    		
    		//nofog command
        	if(word.equalsIgnoreCase("nofog"))
        	{
        		nofog = true;
        		return; //exits method to checkwin then gameboard to display mines
        	}
    		
    		//invalid commands
    		if((!word.equalsIgnoreCase("mark")) && (!word.equalsIgnoreCase("m")) && (!word.equalsIgnoreCase("reveal")) && (!word.equalsIgnoreCase("r")) && (!word.equalsIgnoreCase("guess")) && (!word.equalsIgnoreCase("g")) && (!word.equalsIgnoreCase("help")) && (!word.equalsIgnoreCase("h")) && (!word.equalsIgnoreCase("quit")) && (!word.equalsIgnoreCase("q")))           
    		{
    			commandError();
        		return;
    		}

    		command = word.substring(0, 1);
    	}
    	
    	//quit command
    	if(command.equalsIgnoreCase("q"))
		{
			goodbyeMessage();
		}
    	
    	//help command; display menu, next round happens
    	if(command.equalsIgnoreCase("h"))
    	{
    		String help ="\n" + "Commands Available... \n"
    				+ " - Reveal: r/reveal row col \n"
    				+ " -   Mark: m/mark   row col \n"
    				+ " -  Guess: g/guess  row col \n"
    				+ " -   Help: h/help \n"
    				+ " -   Quit: q/quit";
    		
    		System.out.println(help);
    		
    		//leave method
    		return;
    	}
    	
    	//to parse... [part 2]  if has 2 ints, all good. if has 3, 3rd does error
    	if(scanner.hasNextInt() == true)
		{
			numRow = scanner.nextInt();
			numCol = scanner.nextInt();
		}
   	  	
    	//if user enters out of bounds row,col then error
		if(!isInBounds(numRow, numCol))
		{
			commandError();
			return;
		}
    	
    	//mark command; mark square with 'F'; game updates + next round happens
    	if(command.equalsIgnoreCase("m"))
    	{
    		gameboard[numRow][numCol] = markFlag;
    	}
    	
    	//guess command; mark square with '?'; game updates + next round
    	if(command.equalsIgnoreCase("g"))
    	{
    		gameboard[numRow][numCol] = guessFlag;
    	}
    	
    	//reveal command;
    	if(command.equalsIgnoreCase("r"))
    	{
    		//gameover if square has mine
    		if(gameboardMines[numRow][numCol] == mineMarker)
    		{
    			loseGame();
    		}
    		//reveals how many mines are adjacent
    		else
    		{
    			getNumAdjMines(numRow, numCol);		
    		}
    	}
	
    }
    
    /**
     * Displays the bash prompt "minesweeper-alpha$" and calls userInput() method
     */
    public static void prompt()
    {
    	String prompt = "minesweeper-alpha$ ";
    	System.out.print(prompt);
    	userInput();
    }
    
    /**
     * Indicates whether or not the square is in the game grid.
     *
     * @param row the row index of the square
     * @param col the column index of the square
     * @return bounds true if the square is in the game grid; false otherwise
     */
   private static boolean isInBounds(int row, int col)
    {
    	boolean bounds = true;
    	
    	if((row >= 0) && (row < Minesweeper.rows) && (col >= 0) &&(col < Minesweeper.cols))
    	{
    		bounds = true;
    	}
    	else
    	{
    		bounds = false;
    	}
    	
    	return bounds;
    }
    
    /**
     * Returns the number of mines adjacent to the specified
     * square in the grid. 
     *
     * @param numRow the row index of the square
     * @param numCol the column index of the square
     * @return mineCounter the number of adjacent mines
     */
    private static int getNumAdjMines(int numRow, int numCol)
    {
    	mineCounter = 0;
    	
    	if((isInBounds(numRow, numCol) == true) && !(numCol-1 < 0))
    	{
    		if(gameboardMines[numRow][numCol-1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && (!(numRow-1 < 0) && !(numCol-1 < 0)))
    	{
    		if(gameboardMines[numRow-1][numCol-1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && !(numRow-1 < 0))
    	{
    		if(gameboardMines[numRow-1][numCol] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && (!(numRow-1 < 0) && (numCol+1 < cols)))
    	{
    		if(gameboardMines[numRow-1][numCol+1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && (numCol+1 < cols))
    	{
    		if(gameboardMines[numRow][numCol+1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && ((numRow+1 < rows) && !(numCol-1 < 0)))
    	{
    		if(gameboardMines[numRow+1][numCol-1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && (numRow+1 < rows))
    	{
    		if(gameboardMines[numRow+1][numCol] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	if((isInBounds(numRow, numCol) == true) && (numRow+1 < rows) && (numCol+1 < cols))
    	{
    		if(gameboardMines[numRow+1][numCol+1] == mineMarker)
        	{
        		mineCounter++;
        	}
    	}
    	
    	gameboard[numRow][numCol] = mineCounter;
    	
    	if(gameboardMines[numRow][numCol] != mineMarker)
    	{
    		gameboardMines[numRow][numCol] = mineCounter;
    	}
    	
    	if(mineCounter == 0)
    	{
    		gameboard[numRow][numCol] = reveal;
    	}
    	
    	return mineCounter;
    }
    
    /**
     * method to create and display the gameboard. Initializes the gameboard to 0 and
     * generates mines in random locations when player first begins the game.
     */
    public static void gameboard()
    {
    	//rounds completed
    	System.out.println("\n Rounds Completed: " + roundsCompleted);
    	System.out.println();
    	
    	//initialinzing gameboard array to 0
    	if(initializeArray == true)
    	{
    		gameboard = new int[rows][cols];
    		gameboardMines = new int[rows][cols];
        	//row
        	for(int x = 0; x < gameboard.length; x++)
        	{
        		//col
        		for(int y = 0; y < gameboard[x].length; y++)
        		{
        			gameboard[x][y] = 0;
        			gameboardMines[x][y] = 0;
        		}
        	}
        	
    		initializeArray = false;
    	}
    	
    	/*
    	 * generating random mines [ex: 10x10 grid should have 10 mines]
    	 * if minesweeper(int row, int col) constructor is called
    	 */
    	if((begin == true) && (seedFileGame == false))
    	{
        	Random r = new Random();
        	numOfMines = (int)(Math.ceil((rows * cols) * 0.10));
        	
        	for(int z = 0; z < numOfMines; z++)
        	{
        		//use random number generator to assign mine location to array spaces
        		int rowLocation = r.nextInt(rows);
        		int colLocation = r.nextInt(cols);
        		
        		//generate new random number if mine is already in location
        		if(gameboard[rowLocation][colLocation] == 0)
        		{
        			gameboard[rowLocation][colLocation] = mineMarker;
            		gameboardMines[rowLocation][colLocation] = mineMarker; 
        		}
        		else
        		{
        			rowLocation = r.nextInt(rows);
            		colLocation = r.nextInt(cols);
        			gameboard[rowLocation][colLocation] = mineMarker;
            		gameboardMines[rowLocation][colLocation] = mineMarker; 
        		}
        	}
        
        	begin = false;
    	}
    	
    	//places the mines into the coordinates given in seed file
    	if((begin == true) && (seedFileGame == true))
    	{
    		int count = 0;
    		
    		for(int z = 0; z < numOfMines; z++)
        	{
        		while(seed.hasNext())
        		{
        			int rowLocation = seed.nextInt();
            		int colLocation = seed.nextInt();
            		
            		//error message for when mine location specified is outside the grid
            		if((rowLocation > (rows-1)) || (colLocation > (cols-1)))
            		{
            	    	String error = "Cannot create game with seed1.txt, because it is not formatted correctly. \n";
            	    	System.out.println(error);
            	    	System.exit(0);
            		}
            		else
            		{
            			gameboard[rowLocation][colLocation] = mineMarker;
                		gameboardMines[rowLocation][colLocation] = mineMarker;
                		count+=2;
            		}
            		
            		//check if # lines for mine coordinates exceeds # of mines
            		if(count >= (numOfMines * 2))
            		{
            			if(numOfMines == 1)
            			{
            				break;
            			}
            			break;
            		}
        		} 
        	}
    		begin = false;
    	}
    
    	//printing out the game board
    	//row
    	for(int x = 0; x < gameboard.length; x++)
    	{
    		//printing row numbers
    		System.out.print(" " + x);
    		//col
    		for(int y = 0; y < gameboard[x].length; y++)
    		{
    			//nofog command is true
    			if(nofog == true)
    			{
    				if((isInBounds(x, y-1) == true) && (gameboardMines[x][y-1] == mineMarker))
    				{
    					System.out.print("|");
    				}
    				else
    				{
    					System.out.print(" |");
    				}
    				
        			//prints markers/flags based on the value in square
        			if(gameboard[x][y] == 0)
        			{
        				System.out.print("  ");
        			}
        			else if (gameboard[x][y] == mineMarker)
        			{
        				System.out.print("< >");
        			}
        			else if((gameboard[x][y] == markFlag) && (gameboardMines[x][y] == mineMarker))
        			{
        				System.out.print("<F>");
        			}
        			else if(gameboard[x][y] == markFlag)
        			{
        				System.out.print(" F");
        			}
        			else if((gameboard[x][y] == guessFlag) && (gameboardMines[x][y] == mineMarker))
        			{
        				System.out.print("<?>");
        			}
        			else if(gameboard[x][y] == guessFlag)
        			{
        				System.out.print(" ?");
        			}
        			else if((gameboard[x][y] == mineCounter) && (mineCounter != 0))
        			{
        				System.out.print(" " + gameboardMines[x][y]);
        			}
        			else if(gameboard[x][y] == reveal)
        			{
        				System.out.print(" 0");
        			}
        			else if(gameboardMines[x][y] > 0)
        			{
        				System.out.print(" " + gameboardMines[x][y]);
        			}
        			
        			if((y == gameboard[x].length-1) && (gameboardMines[x][y] == mineMarker))
        			{
        				System.out.print("|");
        			}
        			else if(y == gameboard[x].length-1)
        			{
        				System.out.print(" |");
        			}
    			}
    			//normal game with nofog not enabled
    			else
    			{
    				System.out.print(" |");
        			//prints markers/flags based on the value in square
        			if((gameboard[x][y] == 0) || (gameboard[x][y] == mineMarker))
        			{
        				System.out.print("  ");
        			}
        			else if(gameboard[x][y] == markFlag)
        			{
        				System.out.print(" F");
        			}
        			else if(gameboard[x][y] == guessFlag)
        			{
        				System.out.print(" ?");
        			}
        			else if((gameboard[x][y] == mineCounter) && (mineCounter != 0))
        			{
        				System.out.print(" " + gameboardMines[x][y]);
        			}
        			else if(gameboard[x][y] == reveal)
        			{
        				System.out.print(" 0");
        			}
        			else if(gameboardMines[x][y] > 0)
        			{
        				System.out.print(" " + gameboardMines[x][y]);
        			}
        			
        			if(y == gameboard[x].length-1)
        			{
        				System.out.print(" |");
        			}
    			}
    		}
    		System.out.println();
    	}

    	//printing bottom column numbers
    	for(int y = 0; y == 0; y++)
    	{
    		System.out.print("     " + y);
    	}
    	for(int y = 1; y < cols; y++)
    	{
    		System.out.print("   " + y);
    	}
    	System.out.println();
    	System.out.println();
    	
    	if(nofog == true)
    	{
    		nofog = false;
    	}
    	
    }
    
    /**
     * displays goodbye message and exits program
     */
    public static void goodbyeMessage()
    {
    	String bye = "áƒš(à² _à² áƒš) \n"
    			+ "Y U NO PLAY MORE? \n"
    			+ "Bye!";
    	System.out.println(bye);
    	System.exit(0);
    }
    
    /**
     * errror message for when seed file is not formatted correctly and exits program
     * @param seedFile the text file to run the game
     */
    public static void seedFileError(File seedFile)
    {
    	String fileName = seedFile.getName();
    	String error = "Cannot create game with " + fileName + ", because it is not formatted correctly. \n";
    	System.out.println(error);
    	System.exit(0);
    }
    
    /**
     * displays error message for when user specifies a grid's rows and/or columns outside of the 
     * valid range (0-10) and exits program
     */
    public static void boundsError()
    {
    	String error = "\n à² _à²  says, \"Cannot create a mine field with that many rows and/or columns!\" \n";
    	System.out.println(error);
    	System.exit(0);
    }
    
    /**
     * displays error message for when user enters an invalid command in the userInput method
     */
    public static void commandError()
    {
    	String error = "\n à² _à²  says, \"Command not recognized!\" \n";
    	System.out.print(error);
    }
    
    /**
     * displays the Minesweeper logo when player starts the game
     * @return logo the starting logo string
     */
    public static String startLogo()
    {
    	String logo = "        _ \n"
				+ "  /\\/\\ (_)_ __   ___  _____      _____  ___ _ __   ___ _ __ \n"
				+ " /    \\| | '_ \\ / _ \\/ __\\ \\ /\\ / / _ \\/ _ \\ '_ \\ / _ \\ '__| \n"
				+ "/ /\\/\\ \\ | | | |  __/\\__ \\\\ V  V /  __/  __/ |_) |  __/ | \n"
				+ "\\/    \\/_|_| |_|\\___||___/ \\_/\\_/ \\___|\\___| .__/ \\___|_| \n"
				+ "                                     ALPHA |_| EDITION"
				;
    	return logo;
    }
    
    /**
     * displays the doge logo and player's final score when the player wins the game.
     * Then exits the program.
     */
    public void playerWon()
    {
    	gameboard();
    	//final score
    	score = (rows * cols) - numOfMines - roundsCompleted;
    	
    	String doge = "\n â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–„â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–„â–‘â–‘â–‘â–‘ \"So Doge\" \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–Œâ–’â–ˆâ–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–„â–€â–’â–Œâ–‘â–‘â–‘ \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–Œâ–’â–’â–ˆâ–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–„â–€â–’â–’â–’â–â–‘â–‘â–‘ \"Such Score\" \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–â–„â–€â–’â–’â–€â–€â–€â–€â–„â–„â–„â–€â–’â–’â–’â–’â–’â–â–‘â–‘â–‘ \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–„â–„â–€â–’â–‘â–’â–’â–’â–’â–’â–’â–’â–’â–’â–ˆâ–’â–’â–„â–ˆâ–’â–â–‘â–‘â–‘ \"Much Minesweeping\" \n"
				+ " â–‘â–‘â–‘â–„â–€â–’â–’â–’â–‘â–‘â–‘â–’â–’â–’â–‘â–‘â–‘â–’â–’â–’â–€â–ˆâ–ˆâ–€â–’â–Œâ–‘â–‘â–‘ \n"
				+ " â–‘â–‘â–â–’â–’â–’â–„â–„â–’â–’â–’â–’â–‘â–‘â–‘â–’â–’â–’â–’â–’â–’â–’â–€â–„â–’â–’â–Œâ–‘â–‘ \"Wow\" \n"
				+ " â–‘â–‘â–Œâ–‘â–‘â–Œâ–ˆâ–€â–’â–’â–’â–’â–’â–„â–€â–ˆâ–„â–’â–’â–’â–’â–’â–’â–’â–ˆâ–’â–â–‘â–‘ \n"
				+ " â–‘â–â–‘â–‘â–‘â–’â–’â–’â–’â–’â–’â–’â–’â–Œâ–ˆâ–ˆâ–€â–’â–’â–‘â–‘â–‘â–’â–’â–’â–€â–„â–Œâ–‘ \n"
				+ " â–‘â–Œâ–‘â–’â–„â–ˆâ–ˆâ–„â–’â–’â–’â–’â–’â–’â–’â–’â–’â–‘â–‘â–‘â–‘â–‘â–‘â–’â–’â–’â–’â–Œâ–‘ \n"
				+ " â–€â–’â–€â–â–„â–ˆâ–„â–ˆâ–Œâ–„â–‘â–€â–’â–’â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–’â–’â–’â–â–‘ \n"
				+ " â–â–’â–’â–â–€â–â–€â–’â–‘â–„â–„â–’â–„â–’â–’â–’â–’â–’â–’â–‘â–’â–‘â–’â–‘â–’â–’â–’â–’â–Œ \n"
				+ " â–â–’â–’â–’â–€â–€â–„â–„â–’â–’â–’â–„â–’â–’â–’â–’â–’â–’â–’â–’â–‘â–’â–‘â–’â–‘â–’â–’â–â–‘ \n"
				+ " â–‘â–Œâ–’â–’â–’â–’â–’â–’â–€â–€â–€â–’â–’â–’â–’â–’â–’â–‘â–’â–‘â–’â–‘â–’â–‘â–’â–’â–’â–Œâ–‘ \n"
				+ " â–‘â–â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–‘â–’â–‘â–’â–‘â–’â–’â–„â–’â–’â–â–‘â–‘ \n"
				+ " â–‘â–‘â–€â–„â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–‘â–’â–‘â–’â–‘â–’â–„â–’â–’â–’â–’â–Œâ–‘â–‘ \n"
				+ " â–‘â–‘â–‘â–‘â–€â–„â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–„â–„â–„â–€â–’â–’â–’â–’â–„â–€â–‘â–‘â–‘ CONGRATULATIONS! \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–‘â–€â–„â–„â–„â–„â–„â–„â–€â–€â–€â–’â–’â–’â–’â–’â–„â–„â–€â–‘â–‘â–‘â–‘â–‘ YOU HAVE WON! \n"
				+ " â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–€â–€â–‘â–‘â–‘â–‘â–‘â–‘â–‘â–‘ SCORE: "+ score +"\n";
    	
    	System.out.println(doge);
    	System.exit(0);
    }
    
    /**
     * method to display the gameover logo when the player reveals a mine and exits program
     */
    public static void loseGame()
    {
    	String gameOver = "\n Oh no... You revealed a mine!"
				+ "\n  __ _  __ _ _ __ ___   ___    _____   _____ _ __ \n"
				+ " / _` |/ _` | '_ ` _ \\ / _ \\  / _ \\ \\ / / _ \\ '__| \n"
				+ "| (_| | (_| | | | | | |  __/ | (_) \\ V /  __/ |   \n"
				+ " \\__, |\\__,_|_| |_| |_|\\___|  \\___/ \\_/ \\___|_|   \n"
				+ " |___/     \n\n";
    	System.out.println(gameOver);
    	System.exit(0);
    }


    /**
     * The entry point into the program. This main method does implement some
     * logic for handling command line arguments. If two integers are provided
     * as arguments, then a Minesweeper game is created and started with a 
     * grid size corresponding to the integers provided and with 10% (rounded
     * up) of the squares containing mines, placed randomly. If a single word 
     * string is provided as an argument then it is treated as a seed file and 
     * a Minesweeper game is created and started using the information contained
     * in the seed file. If none of the above applies, then a usage statement
     * is displayed and the program exits gracefully. 
     *
     * @param args the shell arguments provided to the program
     */
    public static void main(String[] args) 
    {
	
	/*
	  The following switch statement has been designed in such a way that if
	  errors occur within the first two cases, the default case still gets
	  executed. This was accomplished by special placement of the break
	  statements.
	*/

	Minesweeper game = null;

	switch (args.length) {

        // random game
	case 2: 

	    int rows, cols;

	    // try to parse the arguments and create a game
	    try {
		rows = Integer.parseInt(args[0]);
		cols = Integer.parseInt(args[1]);
		game = new Minesweeper(rows, cols);
		break;
	    } catch (NumberFormatException nfe) { 
		// line intentionally left blank
	    } // try

	// seed file game
	case 1: 

	    String filename = args[0];
	    File file = new File(filename);

	    if (file.isFile()) {
		game = new Minesweeper(file);
		break;
	    } // if
    
        // display usage statement
	default:

	    System.out.println("Usage: java Minesweeper [FILE]");
	    System.out.println("Usage: java Minesweeper [ROWS] [COLS]");
	    System.exit(0);

	} // switch

	// if all is good, then run the game
	game.run();


    } // main


} // Minesweeper
