# README #

Spring 2016 Project CS1302.

To compile the program, you have to type "javac Minesweeper.java" into the terminal
shell and press 'Enter'. 

There are two ways to run the program. The first is to type 
"java Minesweeper [row] [col]" with any number from 0-10 in the [row] and 
[col] areas (without the brackets). For example, "java Minesweeper 5 5" would
open the program with a 5x5 grid and randomly placed mines.

The second way is to type "java Minesweeper [file]" with the text file name and 
extenstion in the [file] area. For example, 
"java Minesweeper seed1.txt" would open the program with the seed1 file. This 
creates a game with the information written in the seed file. 

To view game commands, type "h".
